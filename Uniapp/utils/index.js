/**
 * 深拷贝
 */
export function deepClone(source) {
    if (!source && typeof source !== 'object') {
      throw new Error('error arguments', 'deepClone')
    }
    const targetObj = source.constructor === Array ? [] : {}
    Object.keys(source).forEach(keys => {
      if (source[keys] && typeof source[keys] === 'object') {
        targetObj[keys] = deepClone(source[keys])
      } else {
        targetObj[keys] = source[keys]
      }
    })
    return targetObj
  }


/**
 * 转成可复制格式
 */
export function handleSymbol(str) {
  let content = str.replaceAll('<br/>','\n')
  content = content.replaceAll('&nbsp;',' ')
  return content;
}

/**
 * 转成可语音模式
 */
export function handleSymbolVoice(str) {
  let content = str.replaceAll('<br/>','')
  content = content.replaceAll('&nbsp;','')
  return content;
}


/**判断是否是手机号**/
export function isPhoneNumber(tel) {
  var reg =/^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
  return reg.test(tel);
}


/**判断是否是pc**/
export function isMobile(){
  try{
    return /(iPhone|iPad|iPod|iOS|Android|Linux armv8l|Linux armv7l|Linux aarch64)/i.test(navigator.platform);

  }catch{
    return true
  }
}

let timeout = null
/**
 * 防抖原理：一定时间内，只有最后一次操作，再过wait毫秒后才执行函数
 *
 * @param {Function} func 要执行的回调函数
 * @param {Number} wait 延时的时间
 * @param {Boolean} immediate 是否立即执行
 * @return null
 */
export function debounce(func, wait = 500, immediate = true) {
  // 清除定时器
  if (timeout !== null) clearTimeout(timeout)
  // 立即执行，此类情况一般用不到
  if (immediate) {
      const callNow = !timeout
      timeout = setTimeout(() => {
          timeout = null
      }, wait)
      if (callNow) typeof func === 'function' && func()
  } else {
      // 设置定时器，当最后一次操作后，timeout不会再被清除，所以在延时wait毫秒后执行func回调方法
      timeout = setTimeout(() => {
          typeof func === 'function' && func()
      }, wait)
  }
}
