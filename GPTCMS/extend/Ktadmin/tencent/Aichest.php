<?php
namespace Ktadmin\Tencent;

use think\facade\Db;
use app\base\model\BaseModel;
use think\facade\Cache;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Asr\V20190614\AsrClient;
use TencentCloud\Asr\V20190614\Models\CreateRecTaskRequest;
use TencentCloud\Asr\V20190614\Models\SentenceRecognitionRequest;
use TencentCloud\Asr\V20190614\Models\DescribeTaskStatusRequest;
use TencentCloud\Common\Profile\HttpProfile;

/**
* 腾讯 Ai百宝箱
**/
class Aichest
{
	protected $secret_id;
	protected $secret_key;
	public $EngineModelType = '16k_zh';
	public $EngSerViceType = '16k_zh';
	public $ChannelNum  = 1;
	public $ResTextFormat = 0;
	public $SourceType = 0; //0url  1数据
	protected $CallbackUrl = '';
	public $VoiceFormat = 'mp3';

    public function __construct($secret_id,$secret_key){
		$this->secret_id = $secret_id;
		$this->secret_key = $secret_key;
	}

	/**
	 * 一句话识别
	 * @param string $url
	 * @return - http response body if succeeds, else false.
	 */
	public function SentenceRecognition($str)
	{
		$TENCENTCLOUD_SECRET_ID = $this->secret_id;
		$TENCENTCLOUD_SECRET_KEY = $this->secret_key;
		$cred = new Credential($TENCENTCLOUD_SECRET_ID, $TENCENTCLOUD_SECRET_KEY);
		// 实例化一个http选项，可选的，没有特殊需求可以跳过
		$httpProfile = new HttpProfile();
		// $httpProfile->setEndpoint();  // 指定接入地域域名(默认就近接入)

		// 实例化一个client选项，可选的，没有特殊需求可以跳过
		$clientProfile = new ClientProfile();
		$clientProfile->setSignMethod("TC3-HMAC-SHA256");  // 指定签名算法(默认为HmacSHA256)
		$clientProfile->setHttpProfile($httpProfile);
		$client = new AsrClient($cred, "ap-shanghai", $clientProfile);
		$req = new SentenceRecognitionRequest();
		$params = [
			"EngSerViceType" => $this->EngSerViceType,
			"VoiceFormat" => $this->VoiceFormat,
			"SourceType" => $this->SourceType,
			"SubServiceType" => 2,
			"ProjectId" => 0,
			"UsrAudioKey" => "test",
		];
		if($this->SourceType == 0) $params['Url'] = $str;
		if($this->SourceType == 1) $params['Data'] = $str;
		// $params = '{"EngSerViceType":"'.$this->EngSerViceType .'","VoiceFormat":'.$this->VoiceFormat.',"SourceType":' . $this->SourceType . ',"Url":" ","Data":"'.$str.'"}';
		$req->fromJsonString(json_encode($params));
		$resp = $client->SentenceRecognition($req);
		return $resp;

	}
	/**
	 * 创建录音文件识别请求
	 * @param string $url
	 * @return - http response body if succeeds, else false.
	 */
	public function CreateRecTask($str = '')
	{
		$TENCENTCLOUD_SECRET_ID = $this->secret_id;
		$TENCENTCLOUD_SECRET_KEY = $this->secret_key;
		$cred = new Credential($TENCENTCLOUD_SECRET_ID, $TENCENTCLOUD_SECRET_KEY);
		// 实例化一个http选项，可选的，没有特殊需求可以跳过
		$httpProfile = new HttpProfile();
		// $httpProfile->setEndpoint();  // 指定接入地域域名(默认就近接入)

		// 实例化一个client选项，可选的，没有特殊需求可以跳过
		$clientProfile = new ClientProfile();
		$clientProfile->setSignMethod("TC3-HMAC-SHA256");  // 指定签名算法(默认为HmacSHA256)
		$clientProfile->setHttpProfile($httpProfile);
		$client = new AsrClient($cred, "ap-shanghai", $clientProfile);
		$req = new CreateRecTaskRequest();
		$params = [
			"EngineModelType" => $this->EngineModelType,
			"ChannelNum" => $this->ChannelNum,
			"ResTextFormat" => $this->ResTextFormat,
			"SourceType" => $this->SourceType,
		];
		if($this->SourceType == 0) $params['Url'] = $str;
		if($this->SourceType == 1) $params['Data'] = $str;
		// $params = '{"EngineModelType":"'.$this->EngineModelType.'","ChannelNum":'.$this->ChannelNum.',"ResTextFormat":'.$this->ResTextFormat.',"SourceType":' . $SourceType . ',"Url":"' . $url . '","CallbackUrl":"","Data":"'.$str.'"}';
		$req->fromJsonString(json_encode($params));
		$resp = $client->CreateRecTask($req);
		return $resp;

	}

	/**
	 * 查看任务状态
	 * @param string $taskid
	 * @return - http response body if succeeds, else false.
	 */
	public function DescribeTaskStatus($taskid)
	{
		$TENCENTCLOUD_SECRET_ID = $this->secret_id;
		$TENCENTCLOUD_SECRET_KEY = $this->secret_key;
		$Endpoint = 'asr.tencentcloudapi.com';
		$cred = '';
		$cred = new Credential($TENCENTCLOUD_SECRET_ID, $TENCENTCLOUD_SECRET_KEY);
		// 实例化一个http选项，可选的，没有特殊需求可以跳过
		$httpProfile = new HttpProfile();
		$httpProfile->setEndpoint($Endpoint);  // 指定接入地域域名(默认就近接入)

		// 实例化一个client选项，可选的，没有特殊需求可以跳过
		$clientProfile = new ClientProfile();
		$clientProfile->setSignMethod("TC3-HMAC-SHA256");  // 指定签名算法(默认为HmacSHA256)
		$clientProfile->setHttpProfile($httpProfile);
		$client = new AsrClient($cred, "ap-shanghai", $clientProfile);
		$req = new DescribeTaskStatusRequest();
		$params = '{"TaskId":'.$taskid.'}';
		$req->fromJsonString($params);
		$resp = $client->DescribeTaskStatus($req);
		return $resp;
	}

}