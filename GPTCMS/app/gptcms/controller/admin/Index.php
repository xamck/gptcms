<?php
declare (strict_types = 1);

namespace app\gptcms\controller\admin;
use app\gptcms\controller\BaseAdmin;

class Index extends BaseAdmin
{
    public function index()
    {
    	return view();
    }
}
